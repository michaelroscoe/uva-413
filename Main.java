/**
 * Created by michaelroscoe on 2016-10-10.
 */
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
public class Main {

    public static void main (String[] args) {

        //Create formatters and input
        NumberFormat format = new DecimalFormat("#0.000000");
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        while (x != 0) {

            //For each case, create necessary logic variables
            ArrayList<Integer> upList = new ArrayList<>();
            ArrayList<Integer> downList = new ArrayList<>();
            int counter = 0;
            int next, current = x;
            int up = 0, down = 0, temp = 0;

            next = sc.nextInt();
            counter++;
            while (next != 0) {

                //within each line, go until 0 is found and add ups and downs
                counter++;
                if (next == current) {
                    temp++;
                } else if (next > current) {
                    up++;
                    if (down != 0) {
                        down += temp;
                        temp = 0;
                        downList.add(down);
                        down = 0;
                    }
                } else if (next < current) {
                    down++;
                    if (up != 0) {
                        up += temp;
                        temp = 0;
                        upList.add(up);
                        up = 0;
                    }
                }
                current = next;
                next = sc.nextInt();

            }
            if (up != 0) {
                up += temp;
                upList.add(up);
            }
            if (down != 0) {
                down += temp;
                downList.add(down);
            }

            //Calculate averages and output the result
            double upAvg = Main.calculateAverage(upList);
            double downAvg = Main.calculateAverage(downList);
            System.out.println("Nr values = " + counter + ":  " + format.format(upAvg) + " " + format.format(downAvg));
            x = sc.nextInt();
        }
    }
    //Method to calculate the averages of the up and down sequences found
    private static double calculateAverage(ArrayList<Integer> list) {
        if (list.isEmpty()) {
            return 0;
        }
        double arraySum = 0;
        for (Integer element: list) {
            arraySum += element;
        }
        return arraySum/list.size();
    }
}
